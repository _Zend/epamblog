﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogApplication.Models
{
    public class Author
    {
        public int Id { get; set; }

        [Display(Name = "Имя автора")]
        public string Name { get; set; }

        [Display(Name = "Статьи автора")]
        public virtual IEnumerable<Article> Articles{ get; set;}

        [Display(Name = "Отзывы автора")]
        public virtual IEnumerable<Feedback> Feedbacks { get; set; }
    }
}