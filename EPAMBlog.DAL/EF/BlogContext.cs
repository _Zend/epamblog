﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EPAMBlog.DAL.Entities;

namespace EPAMBlog.DAL.EF
{
    public class BlogContext:DbContext
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
    }
}