﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPAMBlog.DAL.Entities;

namespace EPAMBlog.DAL.Interfaces
{
    interface IUnitOfWork : IDisposable
    {
        IRepository<Article> Articles { get; }
        IRepository<Author> Authors { get; }
        IRepository<Feedback> Feedbacks { get; }
        void Save();
    }
}
