﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EPAMBlog.DAL.EF;
using EPAMBlog.DAL.Entities;
using EPAMBlog.DAL.Interfaces;

namespace EPAMBlog.DAL.Repositories
{
    public class ArticleRepository : IRepository<Article>
    {
        private readonly BlogContext _db;
        public IEnumerable<Article> GetAll()
        {
            return _db.Articles.ToList();
        }

        public Article Get(int id)
        {
            return _db.Articles.Find(id);
        }

        public IEnumerable<Article> Find(Func<Article, bool> predicate)
        {
            return _db.Articles.Where(predicate).ToList();
        }

        public void Create(Article item)
        {
            _db.Articles.Add(item);
        }

        public void Update(Article item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Article article = _db.Articles.Find(id);
            if (article != null)
                _db.Articles.Remove(article);
        }

        public ArticleRepository(BlogContext blogContext)
        {
            _db = blogContext;
        }
    }
}