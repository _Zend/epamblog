﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace EPAMBlog.DAL.Entities
{
    public class Article
    {
        
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Title { get; set; }

        [Display(Name = "Текст")]
        public string Text { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime PublicationDate { get; set; }

        [Display(Name = "Автор")]
        public virtual Author Author { get; set; }
        public int AuthorId { get; set; }
    }
}