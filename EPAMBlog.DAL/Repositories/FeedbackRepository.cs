﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EPAMBlog.DAL.EF;
using EPAMBlog.DAL.Entities;
using EPAMBlog.DAL.Interfaces;

namespace EPAMBlog.DAL.Repositories
{
    public class FeedbackRepository : IRepository<Feedback>
    {
        private readonly BlogContext _db;
        public IEnumerable<Feedback> GetAll()
        {
            return _db.Feedbacks.ToList();
        }

        public Feedback Get(int id)
        {
            return _db.Feedbacks.Find(id);
        }

        public IEnumerable<Feedback> Find(Func<Feedback, bool> predicate)
        {
            return _db.Feedbacks.Where(predicate).ToList();
        }

        public void Create(Feedback item)
        {
            _db.Feedbacks.Add(item);
        }

        public void Update(Feedback item)
        {
            _db.Entry(item).State= EntityState.Modified;
        }

        public void Delete(int id)
        {
            var item = _db.Feedbacks.Find(id);
            if (item != null)
                _db.Feedbacks.Remove(item);
        }

        public FeedbackRepository(BlogContext blogContext)
        {
            _db = blogContext;
        }
    }
}