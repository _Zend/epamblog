﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EPAMBlog.DAL.Entities
{
    public class Feedback
    {
        public int Id { get; set; }

        [Display(Name = "Текст")]
        [Required]
        public string Text { get; set; }

        [Display(Name = "Дата публикации")]
        [Required]
        public DateTime PublicationDate { get; set; }

        [Display(Name = "Автор")]
        [Required]
        public int AuthorId { get; set; }
        public virtual Author Author { get; set; }
    }
}