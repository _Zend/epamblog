﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EPAMBlog.DAL.EF;
using EPAMBlog.DAL.Entities;
using EPAMBlog.DAL.Interfaces;

namespace EPAMBlog.DAL.Repositories
{
    public class AuthorRepository : IRepository<Author>
    {
        private readonly BlogContext _db;
        public IEnumerable<Author> GetAll()
        {
            return _db.Authors.ToList();
        }

        public Author Get(int id)
        {
            return _db.Authors.Find(id);
        }

        public IEnumerable<Author> Find(Func<Author, bool> predicate)
        {
            return _db.Authors.Where(predicate).ToList();
        }

        public void Create(Author item)
        {
            _db.Authors.Add(item);
        }

        public void Update(Author item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var author = _db.Authors.Find(id);
            if (author != null)
                _db.Authors.Remove(author);
        }

        public AuthorRepository(BlogContext blogContext)
        {
            _db = blogContext;
        }
    }
}