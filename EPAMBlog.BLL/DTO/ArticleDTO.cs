﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace EPAMBlog.BLL.DTO
{
    public class ArticleDTO
    {
        
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDate { get; set; }
        public virtual AuthorDTO Author { get; set; }
        public int AuthorId { get; set; }
    }
}