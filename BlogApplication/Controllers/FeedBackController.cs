﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogApplication.Models;

namespace BlogApplication.Controllers
{
    public class FeedBackController : Controller
    {
        BlogContext db = new BlogContext();
        [HttpGet]
        public ActionResult Index()
        {
            return View(db.Feedbacks.ToList());
        }

        [HttpPost]
        public ActionResult Index(Feedback feedback)
        {
            feedback.PublicationDate = DateTime.UtcNow;
            if (ModelState.IsValid)
            {
                db.Entry(feedback).State = EntityState.Added;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}