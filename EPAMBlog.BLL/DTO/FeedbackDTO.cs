﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EPAMBlog.BLL.DTO
{
    public class FeedbackDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDate { get; set; }
        public int AuthorId { get; set; }
        public virtual AuthorDTO Author { get; set; }
    }
}