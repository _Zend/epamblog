﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EPAMBlog.BLL.DTO
{
    public class AuthorDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual IEnumerable<ArticleDTO> Articles{ get; set;}
        public virtual IEnumerable<FeedbackDTO> Feedbacks { get; set; }
    }
}