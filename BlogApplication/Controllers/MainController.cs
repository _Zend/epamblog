﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogApplication.Models;

namespace BlogApplication.Controllers
{
    public class MainController : Controller
    {
        private BlogContext db = new BlogContext();
        public ActionResult Index()
        {
            return View(db.Articles.OrderByDescending(t => t.PublicationDate).ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(404);
            }
            var article = db.Articles.Find(id);
            return View(article);
        }
    }
}