﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogApplication.Models
{
    public class Feedback
    {
        public int Id { get; set; }

        [Display(Name = "Текст отзыва")]
        [Required]
        public string Text { get; set; }

        [Display(Name = "Дата публикации отзыва")]
        [Required]
        public DateTime PublicationDate { get; set; }

        [Display(Name = "Автор отзыва")]
        [Required]
        public int AuthorId { get; set; }
        public virtual Author Author { get; set; }
    }
}