﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace BlogApplication.Models
{
    public class Article
    {
        
        public int Id { get; set; }

        [Display(Name = "Название статьи")]
        public string Title { get; set; }

        [Display(Name = "Текст статьи")]
        public string Text { get; set; }

        [Display(Name = "Дата публикации статьи")]
        public DateTime PublicationDate { get; set; }

        [Display(Name = "Автор статьи")]
        public virtual Author Author { get; set; }
        public int AuthorId { get; set; }
    }
}