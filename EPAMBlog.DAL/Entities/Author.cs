﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EPAMBlog.DAL.Entities
{
    public class Author
    {
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Статьи")]
        public virtual IEnumerable<Article> Articles{ get; set;}

        [Display(Name = "Отзывы")]
        public virtual IEnumerable<Feedback> Feedbacks { get; set; }
    }
}