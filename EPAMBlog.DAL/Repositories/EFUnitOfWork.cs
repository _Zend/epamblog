﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPAMBlog.DAL.EF;
using EPAMBlog.DAL.Entities;
using EPAMBlog.DAL.Interfaces;

namespace EPAMBlog.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private BlogContext _db;
        private ArticleRepository _articleRepository;
        private AuthorRepository _authorRepository;
        private FeedbackRepository _feedbackRepository;

        private bool _isDisposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                _isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        IRepository<Article> IUnitOfWork.Articles
        {
            get
            {
                if(_articleRepository == null)
                     _articleRepository = new ArticleRepository(_db);
                return _articleRepository;
            }
        }

        IRepository<Author> IUnitOfWork.Authors {
            get
            {
                if (_authorRepository == null)
                    _authorRepository = new AuthorRepository(_db);
                return _authorRepository;
            }
        }

        IRepository<Feedback> IUnitOfWork.Feedbacks {
            get
            {
                if (_feedbackRepository == null)
                    _feedbackRepository = new FeedbackRepository(_db);
                return _feedbackRepository;
            }
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public EFUnitOfWork()
        {
            _db = new BlogContext();
            _isDisposed = false;
        }
    }
}